def signFilter(poi):
    if (poi['id'] == 'Sign' or poi['id'] == 'minecraft:sign') and poi['Text1'] == '[marker]':
        #__import__('pprint').pprint(poi)
        #print("\n".join([poi['Text2'], poi['Text3'], poi['Text4']]))
        return "\n".join([poi['Text2'], poi['Text3'], poi['Text4']])

def playerIcons(poi):
    if poi['id'] == 'Player':
        poi['icon'] = f"https://crafatar.com/renders/head/{poi['uuid']}?overlay=true&scale=3&size=30"  # https://crafatar.com/avatars/
        return f"Last known location for {poi['EntityId']}"



worlds['Sagecraft'] = '/mnt/mc-vanilla-pp/server/world'

outputdir = '/mnt/mc-vanilla-pp/map/gen'

processes = 6


# minzoom = 0
# maxzoom = ?


default_render = {
    'world': 'Sagecraft',
    'texturepath': '/mnt/mc-vanilla-pp/.minecraft/resourcepacks/Dandelion-1.15.zip',
    # 'crop': (-200, -200, 200, 200),
    'markers': [dict(name="Signs", filterFunction=signFilter, checked=True),
                dict(name="Players", filterFunction=playerIcons, checked=True)]
}

end_smooth_lighting = [Base(), EdgeLines(), SmoothLighting(strength=0.5)]
rendermodes = {
    'smooth_lighting': smooth_lighting,
    'smooth_night': smooth_night,
    'cave': cave,
    'nether_smooth_lighting': nether_smooth_lighting,
    'end_smooth_lighting': end_smooth_lighting
}

for rendermode in ['smooth_lighting', 'smooth_night', 'cave', 'nether_smooth_lighting', 'end_smooth_lighting']:
    for dimension in {'smooth_lighting': ['overworld'], 'smooth_night': ['overworld'], 'cave': ['overworld'], 'nether_smooth_lighting': ['nether'], 'end_smooth_lighting': ['end']}[rendermode]:
        for northdirection in ['upper-left', 'upper-right', 'lower-left', 'lower-right']:
            title = f'{dimension} - {rendermode} - {northdirection}'
            renders[title] = {
                **default_render,
                'dimension': dimension,
                'title': title,
                'northdirection': northdirection,
                'rendermode': rendermodes[rendermode]
            }

# __import__('pprint').pprint(renders)
