#!/bin/env python3

# Usage: ./filter_tags.py <input_file> [<tags>, ...] > <output_file>
# -<tag> -> not

import sys
import json


data = json.load(open(sys.argv[1], 'r'))
tags = sys.argv[2:]

to_remove = []

for name, info in data.items():
    for tag in tags:
        if tag[0] == '-':
            if tag in info['tags']:
                to_remove.append(name)
        else:
            if tag not in info['tags']:
                to_remove.append(name)

for name in to_remove:
    del data[name]

print(f'Removed: {", ".join(to_remove)}', file=sys.stderr)
print(json.dumps(data, indent=4))
