#!/bin/env python3

# Usage: ./crawl2html.py <input_file> > <output_file>


COLOR_OLD_VERSIONS = True
TARGET_VERSION = 1.16
MAX_DISTANCE = 0.011
DEFAULT_CHECKED = False


import sys
import json
from jinja2 import Template
from pyquery import PyQuery as pq


template = Template("""<title>curseforge fabric mods dump</title>
<style>
body {
    text-decoration: none !important;
}
h1 {
    display: inline;
}
input[type=checkbox] {
    transform: scale(2);
    width: 30px;
}
details > div {
    border-left: 3px solid gray;
    padding-left: 5px;
}
</style>
<script>
    data = {{ data_json | safe }};

    open_details = (state) =>
        document.querySelectorAll('details').forEach((e) => e.open = state);

    export_json = () => {
        export_obj = {};
        document.querySelectorAll('details').forEach((e) => {
            if (e.querySelector('input[type=checkbox]').checked === true) {
                key = e.id.slice(4);
                export_obj[key] = data[key];}});
        console.log(JSON.stringify(Object.keys(export_obj)) + '\\n\\n\\n\\n' + JSON.stringify(export_obj));
        alert('logged to console');}
</script>
<button onclick="open_details(true);">expand all</button>
<button onclick="open_details(false);">collapse all</button>
<button onclick="export_json();">export</button>
<div>
    {% for name, mod in data.items() %}
    </a></a>
    <details id="mod_{{ mod['slug'] }}">
        <summary>
            <h1 style="color: {{ mod['color'] }};"><input type="checkbox" {% if checked %}checked{% endif %}/>{{ mod['name'] | e }}</h1>
        </summary>
        <a href="https://www.curseforge.com/minecraft/mc-mods/{{ mod['slug'] }}">link</a>
        <p>latest_versions: {{ ', '.join(mod['latest_versions']) }}</p>
        <p>latest_files: {% for name, link in mod['latest_files'].items() %}<a href="https://www.curseforge.com{{link}}">{{name}}</a> {% endfor %}</p>
        <p>licence: <a href="https://www.curseforge.com{{ mod['licence'][1] }}">{{ mod['licence'][0] }}</a></p>
        <p>source: <a href="{{ mod['source'] }}">{{ mod['source'] }}</a></p>
        <p>tags: {% for tag in mod['tags'] %}<a href="https://www.curseforge.com/minecraft/mc-mods/{{tag}}">{{tag}}</a> {% endfor %}</p>
        <p>dependencies: {% for name in mod['dependencies'] %}<a href="#mod_{{name}}">{{name}}</a> {% endfor %}</p>
        <p>id: {{ mod['id'] }}</p>
        <div>{{ mod['description'] | safe }}</div>
    </details>
    {% endfor %}
</div>""")


with open(sys.argv[1], 'r') as f:
    input_str = f.read()
    if not input_str.strip(' \n').endswith('}'):
        input_str = input_str[:-4]

input_str += (input_str.count('{') - input_str.count('}')) * '}'
data = json.loads(input_str)

for slug, mod in data.items():
    description_new = ''
    for line in mod['description'].split('\n'):
        if '<iframe ' in line:
            line = line.replace('<iframe ', '<a ').replace('/>', '>video</a>').replace('src="', 'href="')
        elif '<a ' in line:
            line = line.replace('href="/linkout', 'href="https://www.curseforge.com/linkout')
        description_new += line + '\n'
    mod['description'] = pq(description_new).outer_html()

    mod['color'] = 'inherit'
    if COLOR_OLD_VERSIONS:
        versions_int = [float(version) for version in mod['latest_versions']]
        distance = min([abs(TARGET_VERSION - version) for version in versions_int])
        if distance == 0:
            mod['color'] = 'green'
        elif distance <= MAX_DISTANCE:
            mod['color'] = 'orange'
        else:
            mod['color'] = 'red'

print(template.render(data=data, data_json=json.dumps(data), checked=DEFAULT_CHECKED))
