#!/bin/env python3

# Usage: ./dowload_mods.py <input_file> [<mods_folder>]
# mods_folder defaults to .minecraft/mods


TARGET_VERSION = '1.15.2'


from pyquery import PyQuery as pq
import logging
import argparse
import json
import sys
import os
from selenium import webdriver, common


def get(url):
    tmp_no_reload = False

    while True:
        if not tmp_no_reload:
            ff.get(url)
        else:
            tmp_no_reload = False

        html = ff.find_element_by_xpath("//*").get_attribute('outerHTML')

        if 'Attention Required!' in pq(html).find('title').text():
            logger.warning('User interaction required, press enter')
            input()

            tmp_no_reload = True
            continue
        return html


parser = argparse.ArgumentParser(description='Downloads mods from curseforge')
parser.add_argument('input_file', type=str, help='Path to json list of mods')
parser.add_argument('-p', '--profile', type=str, default=None, help='Path to FF profile to be used')
parser.add_argument('-o', '--output', type=str, default='.minecraft/mods', help='Path to output folder, defaults to .minecraft/mods')
parser.add_argument('-v', '--verbose', action="store_true", help='Enable debug logging to stderr')

args = parser.parse_args()


logging.basicConfig()
logger = logging.getLogger(__name__)
if args.verbose:
    logger.setLevel(logging.DEBUG)


logger.debug(args)


profile = webdriver.FirefoxProfile(args.profile)
profile.set_preference("browser.download.folderList", 2)
profile.set_preference("browser.download.manager.showWhenStarting", False)
profile.set_preference("browser.download.dir", os.path.realpath(args.output))
profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/x-amz-json-1.0")
ff = webdriver.Firefox(firefox_profile=profile)


data = json.load(open(args.input_file, 'r'))

failed = []

for i, mod in enumerate(data.items()):
    name, info = mod

    logger.debug(f'mod: {name} ({i + 1}/{len(data)})')

    page = pq(get(f'https://www.curseforge.com/minecraft/mc-mods/{name}/files/all'))

    files = {}
    for th in page.find('.listing tr')[1:]:
        tds = pq(th).find('td')
        files[pq(tds[1]).text()] = {'type': pq(tds[0]).text(),
                                    'date': int(pq(tds[3]).find('abbr').attr('data-epoch')),
                                    'version': pq(tds[4]).find('div > div').text(),
                                    'link': pq(tds[6]).find('a').attr('href') + '/file'}

    selected = None
    for name, file in sorted(files.items(), key=lambda item: item[1]['date']):
        if file['version'] == TARGET_VERSION:
            selected = file
            break

    if selected is None:
        print('\033[91mERROR\033[0m: No valid version found', file=sys.stderr)

        failed.append(info['slug'])
    else:
        print(f'Selected: {selected}', file=sys.stderr)

        #ff.set_page_load_timeout(1)  # Download start doesn't trigger loaded successfully
        #try:
        #    ff.get('https://www.curseforge.com' + file['link'])
        #except common.exceptions.TimeoutException:
        #    pass
        #ff.set_page_load_timeout(30)

print(f'Failed: {" ".join(failed)}', file=sys.stderr)
input('All download finished? (press enter)', file=sys.stderr)
ff.close()
